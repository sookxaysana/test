using System.Collections.Generic;
using UnityEngine;
using RPG.Utility;
using UnityEngine.InputSystem;
using RPG.Core;
namespace RPG.Quest
{
    public class TreasureChest : MonoBehaviour
    {
        [SerializeField]private QuestItemSO questItem;
        public Animator animatorCmp;
        private bool isInteractable = false; 
        private bool hasBeenOpened = false;
        private void Start()
        {
            if (PlayerPrefs.HasKey("PlayerItems"))
            {
                List<string> playerItems = PlayerPrefsUtility.GetString("PlayerItems");
                playerItems.ForEach(checkItem);
            }
        }
        private void OnTriggerEnter()
        {
            //print("player detected!");
            isInteractable = true;
        }
        private void OnTriggerExit()
        {
            isInteractable = false;
        }
        public void HandleInteract(InputAction.CallbackContext context)
        {
            if (!isInteractable || hasBeenOpened || !context.performed) return;
            EventManager.RaiseTrasureChestUnlocked(questItem, true);
            //print("open box");
            animatorCmp.SetBool(
              Constants.IS_SHAKING_ANIMATOR_PARAM  , false
              );
            hasBeenOpened = true;
            AudioSource audioSourceCmp = GetComponent<AudioSource>();
            if (audioSourceCmp.clip == null ) return;
            audioSourceCmp.Play();

        }
        private void checkItem(string itemName)
        {
            if(itemName != questItem.name) return;

            hasBeenOpened = true;
            animatorCmp.SetBool(Constants.IS_SHAKING_ANIMATOR_PARAM,false);

            EventManager.RaiseTrasureChestUnlocked(questItem, false);
        }
    }
}

