using System;
using UnityEngine;

namespace RPG.Example
{
    public class Robot : MonoBehaviour
    {
        private BatteryRegulations includedBattery;
        public Robot()
        {
            includedBattery = new Battery(80f);
            includedBattery.CheckHealth();
            Charger.ChargeBattery(includedBattery);
            includedBattery.CheckHealth();
            print(Charger.chargerInUse);
        }
    }

    public class Battery : BatteryRegulations
    {
        public Battery(float newHealth) :base(newHealth) 
        {
           
        }

        public override void CheckHealth()
        {
            Debug.Log(health);
        }
    }
    static class Charger
    {
        public static bool chargerInUse = false;
        public static void ChargeBattery(BatteryRegulations batteryToCharge)
        {
            chargerInUse = true;
            batteryToCharge.health = 100f;
        }
    }
    public abstract class BatteryRegulations
    {
        public float health;
        public BatteryRegulations(float newHealth)
        {
            health = newHealth;
            Debug.Log("New battery created!");
        }
        public abstract void CheckHealth();
    }
  //  public class Robot : MonoBehaviour
  //{

    /*public int age = 5;
    public float price = 99.99f;
    public bool isTurnedOn =false;
    public Robot()
    {
        float newPrice = CalculatePrice(0.1f, 1);
        if (newPrice > 75f)
        {
            price = newPrice;
        }
        else 
        {*/
    /*print("Price is too low");
    Debug.Log("Price is too low");
    Debug.LogWarning("Price is too low");
    Debug.LogError("Price is too low");*/
    /*  Log("Price is too low");
      Log(age);
      Log(isTurnedOn);

  }

}
[Obsolete("CalcuatePrice() is deprecated. Use ApplyDiscount() instead.")]
public float CalculatePrice(float discount, int quantity)
{
  return (price - (price * discount)) * quantity;
}
public float ApplyDiscount(float discount)
{
  return (price - (price * discount));
}
public void Log<T>(T message)
{
  Debug.Log(message);
}*/
    //}
}