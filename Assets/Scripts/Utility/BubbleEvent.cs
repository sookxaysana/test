using UnityEngine;
using UnityEngine.Events;
namespace RPG.Utility
{
    public class BubbleEvent : MonoBehaviour
    {
        public event UnityAction OnBubbleStartAttack = () => 
        {
        };
        public UnityAction OnBubbleCompleteAttack = () => { };
        public UnityAction OnBubbleHit = () => { };
        public UnityAction OnbubbleCompleteDefeat = () => { };  
        private void OnStartAttack()
        {
           // print("Star Attack");
           OnBubbleStartAttack.Invoke();
        }
        private void OnCompleteAttack()
        {
           // print("Complete Attack");
           OnBubbleCompleteAttack.Invoke();
        }
        private void OnHit()
        {
            OnBubbleHit.Invoke();
        }
        private void OnCompleteDefeat()
        {
            OnbubbleCompleteDefeat.Invoke();
        }
    }
}

