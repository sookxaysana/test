using UnityEngine;
namespace RPG.Character
{
    public class AIAttackState : AIBaseState
    {
        public override void EnterState(EnemyController enemy)
        {
            enemy.movementCmp.StopMovingAgent();
           // Debug.Log("entering attack state");
        }
        public override void UpdateState(EnemyController enemy)
        {
            if(enemy.player == null)
            {
                enemy.combatCmp.CancelAttack();
                return;
            }
            if (enemy.distanceFromPlayer > enemy.attackRange) 
            {
                enemy.combatCmp.CancelAttack();
                enemy.SwithchState(enemy.chaseState);
                return;
            
            }
            if(enemy.hasUIOpened)return;

            //Debug.Log("attacking player");
            enemy.combatCmp.StartAttack();
            enemy.transform.LookAt(enemy.player.transform);
        }
    }
} 