
using UnityEngine;
namespace RPG.Character
{
    public class AIReturnState : AIBaseState
    {
        Vector3 targetPosition;
        public override void EnterState(EnemyController enemy)
        {
            //Debug.Log("Entering return state");
            enemy.movementCmp.UpdateAgentSpeed(enemy.stats.walkSpeed, true);

            if (enemy.patrolCmp != null)
            {
                targetPosition = enemy.patrolCmp.GetNextPosition();
                enemy.movementCmp.MoveAgentByDestination(targetPosition);
            }
            else
            {
                enemy.movementCmp.MoveAgentByDestination(
                    enemy.originalPositon
                     );
            }

        }
        public override void UpdateState(EnemyController enemy)
        {
            if (enemy.distanceFromPlayer < enemy.chaseRange)
            { 
                enemy.SwithchState(enemy.chaseState);
                return;
            }
            if (enemy.movementCmp.ReachedDestination())
            {
                if (enemy.patrolCmp != null)
                {
                    enemy.SwithchState(enemy.patrolState);
                    return;
                }
                else
                {
                    enemy.movementCmp.isMoving = false;
                    enemy.movementCmp.Rotate(enemy.movementCmp.originalForwardVector);
                }

            }
            else
            {
                if (enemy.patrolCmp != null)
                {
                    Vector3 newForwardVector = targetPosition - enemy.transform.position;
                    newForwardVector.y = 0;
                    enemy.movementCmp.Rotate(newForwardVector);
                }
                else
                {
                    Vector3 newForwardVector = enemy.originalPositon - 
                        enemy.transform.position;
                    newForwardVector.y = 0;
                    enemy.movementCmp.Rotate(newForwardVector);
                }
            }

        }
    }
}