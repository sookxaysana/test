using RPG.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;

namespace RPG.UI
{
    public class UIPauseState : UIBaseState
    {
        public  UIPauseState(UIController uiController) : base(uiController) { }
        public override void EnterState()
        {
            PlayerInput playerInputCmp = GameObject.FindGameObjectWithTag(
                Constants.GAME_MANAGER_TAG
                ).GetComponent<PlayerInput>();

            VisualElement pauseContainer = controller.root
                .Q<VisualElement>("pause-container");
            playerInputCmp.SwitchCurrentActionMap(
                    Constants.UI_ACTION_MAP
                );
            pauseContainer.style.display = DisplayStyle.Flex;
            Time.timeScale = 0;

        }
        public override void SelectButton()
        {

        }
    }

}

