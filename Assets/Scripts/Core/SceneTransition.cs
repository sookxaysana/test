using UnityEngine.SceneManagement;
using System.Collections;
using RPG.Utility;
using UnityEngine;
namespace RPG.Core
{
    public class SceneTransition
    {
        public static IEnumerator Initiate(int sceneIndex)
        {
            AudioSource audioSourceCmp = GameObject.FindGameObjectWithTag(
                    Constants.GAME_MANAGER_TAG
                ).GetComponent<AudioSource>();
            float duration = 2f;
            while (audioSourceCmp.volume > 0)
            {
                audioSourceCmp.volume -= Time.deltaTime / duration;
                yield return new WaitForEndOfFrame();
            }
            SceneManager.LoadScene(sceneIndex);
        }
    }
}

