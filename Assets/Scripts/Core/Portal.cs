
using UnityEngine;
using RPG.Utility;
using System.Runtime.CompilerServices;
namespace RPG.Core
{
    public class Portal : MonoBehaviour
    {
        [SerializeField]private int nextSceneIndex;
        public Transform spawnPoint;
        private Collider colliderCmp;
        private void Awake()
        {
            colliderCmp = GetComponent<Collider>();
        }
        private void OnTriggerEnter(Collider other)
        {
            
            if (!other.CompareTag(Constants.PLAYER_TAG)) return;
            //print("play detacted");
            colliderCmp.enabled = false;
            EventManager.RaisePortalEnter(other, nextSceneIndex);
            StartCoroutine(SceneTransition.Initiate(nextSceneIndex));
        }
    }

}

